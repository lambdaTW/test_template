from time import sleep
from selenium.webdriver.common.action_chains import ActionChains


class BasePage:
    """
    Base class to initialize the base page that will be called from all pages
    """
    CLICK_WAIT_TIME = 2

    def __init__(self, driver=None, base_url=None):
        self.base_url = base_url
        if driver:
            self.driver = driver
            self.action_chains = ActionChains(self.driver)
        else:
            raise TypeError(
                f"No driver provide for {self}"
            )
        if self.base_url:
            self.driver.get(self.base_url)

    def click(self, element):
        self.action_chains.move_to_element(element).click(element).perform()
        sleep(self.CLICK_WAIT_TIME)

    def double_click(self, element):
        self.action_chains.double_click(element).perform()

    def home(self):
        '''
        Change page to Google to avoid cache, and reGET *self.base_url*
        '''
        self.driver.get('http://www.google.com')
        self.driver.get(self.base_url)
