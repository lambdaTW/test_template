from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains


class BasePageElement:
    """Base page class that is initialized on every page object class."""
    locator = None

    def __set__(self, obj, value):
        """Sets the element to the value supplied"""
        driver = obj.driver
        WebDriverWait(driver, 100).until(
            EC.presence_of_element_located(self.locator))
        element = driver.find_element(*self.locator)
        if element.tag_name == 'select':
            Select(element).select_by_value(value)
        elif (element.tag_name == 'input' and
              element.get_attribute('type') == 'date'):
            action_chains = ActionChains(driver)
            action_chains.move_to_element(
                element
            ).click().send_keys(value).perform()
        else:
            element.send_keys(value)

    def __get__(self, obj, owner):
        """Gets the element of the specified object"""
        driver = obj.driver
        WebDriverWait(driver, 100).until(
            EC.presence_of_element_located(self.locator))
        element = driver.find_element(*self.locator)
        return element


class WaitAndGetElement(BasePageElement):
    """This class gets the search element from the specified locator"""

    def __init__(self, locator):
        self.locator = locator
