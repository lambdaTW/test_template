import os
from pathlib import Path
from datetime import datetime

from django.conf import settings
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

from selenium import webdriver
from selenium.webdriver.firefox.options import Options


class FunctionalTest(StaticLiveServerTestCase):
    SCREEN_DUMP_LOCATION = Path(str(settings.BASE_DIR.path('screendumps')))

    def setUp(self):
        options = Options()
        # Run headless mode on CI and set the screen with 1920*1080
        if os.environ.get('CI_SERVER', False):
            options.add_argument("-headless")
            options.add_argument("--width=1920")
            options.add_argument("--height=1080")
        self.driver = webdriver.Firefox(options=options)

    def tearDown(self):
        if any(error for (_, error) in self._outcome.errors):
            # Create screen dump path if not exist
            self.SCREEN_DUMP_LOCATION.mkdir(parents=True, exist_ok=True)
            for index, handle in enumerate(self.driver.window_handles):
                self.window_id = index
                self.driver.switch_to_window(handle)
                self.dirver_take_screenshot()
                self.driver_dump_html()
        self.driver.quit()
        super().tearDown()

    def dirver_take_screenshot(self):
        screenshot_file = self.get_dump_filename(file_extension='png')
        self.driver.get_screenshot_as_file(screenshot_file)

    def driver_dump_html(self):
        filename = self.get_dump_filename(file_extension='html')
        with open(filename, 'w') as html_file:
            html_file.write(self.driver.page_source)

    def get_dump_filename(self, file_extension: str = 'txt'):
        now = datetime.now().isoformat()
        return (
            f'{self.dump_dir}/'
            f'{self.__class__.__name__}.{self._testMethodName}-'
            f'window-{self.window_id}-'
            f'{now}.{file_extension}'
        )
