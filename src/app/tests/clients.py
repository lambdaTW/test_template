from django.test.client import Client


class PayloadClient(Client):

    def delete(self, *args, **kwargs):
        """ Construct a DELETE request that includes data. """
        kwargs.update({'REQUEST_METHOD': 'DELETE'})
        kwargs.update({'content_type': 'application/json'})
        return super().put(*args, **kwargs)

    def patch(self, *args, **kwargs):
        kwargs.update({'REQUEST_METHOD': 'PATCH'})
        kwargs.update({'content_type': 'application/json'})
        return super().put(*args, **kwargs)

    def put(self, *args, **kwargs):
        kwargs.update({'REQUEST_METHOD': 'PUT'})
        kwargs.update({'content_type': 'application/json'})
        return super().put(*args, **kwargs)

    def post(self, *args, **kwargs):
        kwargs.update({'content_type': 'application/json'})
        return super().post(*args, **kwargs)

    def get(self, *args, **kwargs):
        kwargs.update({'content_type': 'application/json'})
        return super().get(*args, **kwargs)
