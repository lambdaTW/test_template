Django Test Template
===
This repository is a simple Django project for practicing how to write tests.

## Usage
### (`Do once`) Create a virtual environment
```shell-script
python3 -m venv venv
```
### Enter the virtual environment
```shell-script
source venv/bin/activate
```
### Install package
```shell-script
pip install -r requirements/base.txt
```
### Install develop package
```shell-script
pip install -r requirements/develop.txt
```
### Run tests
```shell-script
cd src
python3 manage.py test
```

### Create Application
```shell-script
cd src/app
django-admin startapp <application name>
```

### Optional
#### Install new package & update requirements
```shell-script
# Intall
pip install <package_name>

# Get installed packages
pip freeze -r requirements/base.txt -r requirements/develop.txt

# Edit requirements
emacs -nw <requirements/base.txt or requirements/develop.txt>
```
